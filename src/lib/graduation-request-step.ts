import { Type } from '@angular/core';

/**
 *
 * GraduationRequestStep is a wrapper to be used around components that they
 * can be added as steps at the graduation request wizard.
 *
 */


export class GraduationRequestStep {
  constructor(public component: Type<any>, public data: any) {}
}

