import { Component, OnInit, OnDestroy, Input, ViewChild, Output, EventEmitter, TemplateRef } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { NgForm } from '@angular/forms';
import { ModalService } from '@universis/common';

@Component({
  selector: 'universis-graduation-document-submission',
  templateUrl: './graduation-document-submission.component.html'
})
export class GraduationDocumentSubmissionComponent implements OnInit, OnDestroy {

  private alternateName = 'graduationDocumentsSubmission';

  /**
   * The overall graduation request status
   */
  @Input() graduationRequestStatusObservable$: BehaviorSubject<any>;

  /**
   * Notifies that a file should be uploaded
   */
  @Output() uploadFile = new EventEmitter();

  /**
   * Notifies that a file should be uploaded
   */
  @Output() outputAction = new EventEmitter();

  /**
   * The file upload form
   */
  @ViewChild('studentGraduationRequestActionForm') studentGraduationRequestActionForm: NgForm;

  /**
   * The delete attachment modal template
   */
  @ViewChild('DeleteAttachmentModalTemplate') modalTemplate: TemplateRef<any>;

  /**
   * The delete attachment modal reference
   */
  modalRef;

  /**
   * The subscription to the graduation status object
   */
  graduationRequestStatusSubscription: Subscription;

  /**
   * The overall graduation request status
   */
  public graduationRequestStatus;

  /**
   * The list of documents to show
   */
  public documents = [];

  /**
   * The list of document types for the student to submit
   */
  public attachmentTypes = [];

  /**
   * The available attachments
   */
  public attachments = [];

  /**
   * A notice about the status of the documents submission
   */
  public statusNotice: string;

  /**
   * The list of the forms that the student can submit
   */
  public forms = [];

  /**
   * The id of the student action
   */
  public graduationRequestId: number;

  /**
   * The index of the document to be deleted
   */
  public documentIndex: number;

  public deleteCandidate;

  constructor(
    private _translateService: TranslateService,
    private _modalService: ModalService
  ) { }

  public messageBlocksCollapseState: Array<boolean>;

  ngOnInit() {
    this.messageBlocksCollapseState = new Array(this.documents.length).fill(true);

    this.graduationRequestStatusObservable$.subscribe((graduationStatus) => {
      this.graduationRequestStatus = graduationStatus;
      const currentStep = this.getStep(this.graduationRequestStatus, this.alternateName);

      if (currentStep.status) {
        this.statusNotice = this._translateService.instant(
          `UniversisGraduationModule.DocumentsSubmission.SubmissionStatuses.${currentStep.status}`
        );
      }

      if (currentStep && currentStep.data) {
        const data = currentStep.data;
        this.graduationRequestId = data.graduationRequestId;

        if (data.attachments) {
          this.attachments = data.attachments;
          this.attachments.forEach((attachment) => {
            const formData: FormData = new FormData();
            formData.append('file[attachmentType]', attachment.requestActionType.attachmentType);
            const attachmentInfo = {
              form: formData,
              selectedFileName: undefined,
              attachmentType: undefined
            };

            this.forms.push(attachmentInfo);
          });
        }

        if (data.errors) {
          for (const actionKey of Object.keys(data.errors)) {
            for (const alternateName of Object.keys(data.errors[actionKey])) {
              this.attachments.forEach(attachment => {
                if (attachment.alternateName === alternateName) {
                  attachment.errorMessage = this._translateService.instant(
                    `UniversisGraduationModule.DocumentsSubmission.Errors.${actionKey}`
                  );
                }
              });
            }
          }
        }
      }
    });
  }

  ngOnDestroy() {
    if (this.graduationRequestStatusSubscription) {
      this.graduationRequestStatusSubscription.unsubscribe();
    }
  }

  /**
   *
   * Triggers a file download
   *
   * @param attachmentTypeIndex the index of the attachment to download
   *
   */
  public async downloadFile(attachmentTypeIndex: number) {
    // TODO: is it being used?
    this.outputAction.emit({
      action: 'download',
      data: {
        attachmentAlternateName: this.attachments[attachmentTypeIndex].document.alternateName,
        attachmentTypeAlternateName: this.attachments[attachmentTypeIndex].alternateName,
      }
    });
  }

  /**
   *
   * Emits a file upload event
   *
   * @param attachmentTypeIndex The index of the attachment type
   *
   */
  public async triggerUploadFile(attachmentTypeIndex: number) {
    if (!this.forms[attachmentTypeIndex] || !this.forms[attachmentTypeIndex].form.get('file')) {
      console.warn('Graduation document upload attempt with empty file.');
    } else {
      this.outputAction.emit({
        action: 'upload',
        data: {
          eventId: this.graduationRequestId,
          form: this.forms[attachmentTypeIndex].form,
          attachmentType: this.attachments[attachmentTypeIndex].alternateName
        }
      });
    }
  }

  /**
   *
   * Emits a file remove event
   *
   * @param attachmentTypeIndex
   *
   */
  public async triggerRemoveFile(attachmentTypeIndex: number) {
    if (!this.attachments || !this.attachments[attachmentTypeIndex]) {
      console.warn('Graduation document remove attempt with out of scope index');
    } else if (!this.attachments[attachmentTypeIndex].document) {
      console.warn('Graduation document remove attempt with undefined document');
    } else if (this.attachments[attachmentTypeIndex].document) {

      if (this.forms[attachmentTypeIndex].form.get('file')) {
        this.forms[attachmentTypeIndex].form.delete('file');
        this.forms[attachmentTypeIndex].selectedFilename = undefined;
      }

      this.outputAction.emit({
        action: 'remove',
        data: {
          requestId: this.graduationRequestId,
          attachmentId: this.attachments[attachmentTypeIndex].document.id,
          attachmentTypeAlternateName: this.attachments[attachmentTypeIndex].alternateName
        }
      });
    } else {
      console.warn('Graduation document remove attempt failed');
    }
  }

  /**
   *
   * Event handler for the file change event
   *
   * @param event The event as produced by the browser
   * @param attachmentTypeIndex The index of the attachment type
   *
   */
  onFileChange(event, attachmentTypeIndex: number) {
    if (event.target.files && event.target.files.length > 0) {
      const file = (event.target as HTMLInputElement).files[0];
      this.forms[attachmentTypeIndex].form.delete('file');
      this.forms[attachmentTypeIndex].selectedFilename = file.name;
      this.forms[attachmentTypeIndex].form.append('file', file, file.name);
    } else {
      console.warn('Graduation attachment upload attempt without file for ', this.forms[attachmentTypeIndex].attachmentType.id);
    }
  }

  /**
   *
   * Opens the delete modal and set the current document index
   *
   * @param attachmentIndex The index of the current attachment (request attachment)
   *
   */
  openDeleteModal(attachmentIndex: number) {
    if (typeof attachmentIndex === 'number') {
      this.deleteCandidate = {
        index: attachmentIndex,
        type: this.attachments[attachmentIndex].name
      };
      this.modalRef = this._modalService.openModal(this.modalTemplate);
    } else {
      console.warn('Attempt to delete document without index');
    }
  }

  /**
   *
   * Close the modal without any other action
   *
   */
  closeDeleteModal() {
    this.modalRef.hide();
  }

  /**
   *
   * Calls the file removal for the current document
   *
   */
  confirmDocumentDelete() {
    this.triggerRemoveFile(this.deleteCandidate.index);
    this.closeDeleteModal();
  }

  /**
   *
   * Given an alternateName, gets the the step
   *
   * @param graduationRequestStatus The graduation request status data
   * @param alternateName The alternate name of the step
   *
   */
  getStep(graduationRequestStatus, alternateName: string) {
    return graduationRequestStatus.steps.find(
      (step) => new RegExp(alternateName, 'gi').test(step.alternateName)
    );
  }
}
