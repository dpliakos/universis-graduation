import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';

// import { LocalizedDatePipe } from '@universis/common';

@Component({
  selector: 'universis-graduation-ceremony',
  templateUrl: './graduation-ceremony.component.html'
})
export class GraduationCeremonyComponent implements OnInit, OnDestroy {

  /**
   * A unique identifier for the graduation process step
   */
  private alternateName = 'graduationCeremony';

  /**
   * The handles the communication with the parent component
   */
  @Input() graduationRequestStatusObservable$: BehaviorSubject<any>;

  /**
   * The overall graduation request status
   */
  graduationRequestStatusSubscription: Subscription;

  /**
   * The overall graduation request status
   */
  public graduationRequestStatus;

  /**
   * A list of certificates for the student.
   */
  public certificates: Array<any>;

  /**
   * The status of the graduation ceremony as a step in the graduation process
   */
  public status;

  /**
   * The date of graduation ceremony
   */
  public graduationCeremonyDate: Date;

  /**
   * A message for the student in case of failed graduation
   */
  public message: string;


  /**
   * The translated name of location of the graduation ceremony
   */
  public graduationCeremonyLocation;


  constructor() { }

  ngOnInit() {
    this.graduationRequestStatusSubscription = this.graduationRequestStatusObservable$.subscribe((graduationStatus) => {
      this.graduationRequestStatus = graduationStatus;
      const currentStep = this.getStep(this.graduationRequestStatus, this.alternateName);
      this.status = currentStep.status ? currentStep.status : 'available';

      if (currentStep.data) {
        this.certificates = currentStep.data.certificates;
        this.graduationCeremonyDate = currentStep.data.ceremonyDate;
        this.graduationCeremonyLocation = currentStep.data.location;
      }

      if (this.status === 'failed') {
        this.message = currentStep.data.message;
      }
    });
  }

  ngOnDestroy() {
    this.graduationRequestStatusSubscription.unsubscribe();
  }


  downloadCertificate() {

  }

  /**
   *
   * Given an alternateName, gets the the step
   *
   * @param graduationRequestStatus The graduation request status data
   * @param alternateName The alternate name of the step
   *
   */
  getStep(graduationRequestStatus, alternateName: string) {
    return graduationRequestStatus.steps.find(
      (step) => new RegExp(alternateName, 'gi').test(step.alternateName)
    );
  }
}
