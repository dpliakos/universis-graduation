import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraduationStudentsGraduationTabComponent } from './graduation-students-graduation-tab.component';

describe('GraduationStudentsGraduationTabComponent', () => {
  let component: GraduationStudentsGraduationTabComponent;
  let fixture: ComponentFixture<GraduationStudentsGraduationTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraduationStudentsGraduationTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraduationStudentsGraduationTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
