import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraduationStudentsHomeComponent } from './graduation-students-home.component';

describe('GraduationStudentsHomeComponent', () => {
  let component: GraduationStudentsHomeComponent;
  let fixture: ComponentFixture<GraduationStudentsHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraduationStudentsHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraduationStudentsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
