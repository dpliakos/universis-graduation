import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraduationStudentsRequestWrapperComponent } from './graduation-students-request-wrapper.component';

describe('GraduationStudentsRequestWrapperComponent', () => {
  let component: GraduationStudentsRequestWrapperComponent;
  let fixture: ComponentFixture<GraduationStudentsRequestWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraduationStudentsRequestWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraduationStudentsRequestWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
