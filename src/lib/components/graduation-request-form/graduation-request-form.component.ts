import { Component, OnInit, OnDestroy, Output, ViewChild, EventEmitter, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Subscription, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'universis-graduation-request-form',
  templateUrl: './graduation-request-form.component.html',
  styleUrls: ['./graduation-request-form.component.scss']
})
export class GraduationRequestFormComponent implements OnInit, OnDestroy {

  private alternateName = 'graduationRequest';

  /**
   *
   * The graduation request special request form
   *
   */
  @ViewChild('studentGraduationRequest') studentGraduationRequest: NgForm;

  /**
   * The overall graduation request status
   */
  @Input() graduationRequestStatusObservable$: BehaviorSubject<any>;

  /**
   * Notifies that there is a form submission
   */
  @Output() formSubmit = new EventEmitter();

  /**
   * The overall graduation request status
   */
  public graduationRequestStatus;

  /**
   * Subscription to the graduation status
   */
  graduationRequestStatusSubscription: Subscription;

  /**
   *
   * States whether the student has already submitted the form
   *
   */
  public submitted: boolean;

  /**
   *
   * Dictates if a graduation request can be submitted
   *
   */
  public canSubmit = false;

  /**
   *
   * A list of items that the student can consent to
   *
   */
  public graduationRequestConsents = [];

  /**
   *
   * The status of the checkboxes
   *
   */
  public consents = new Array(this.graduationRequestConsents.length).fill(false);

  /**
   *
   * The name of the request. This message is completed by the application
   *
   */
  public requestName: string;

  /**
   *
   * A message that the student can send alongside their graduation request
   *
   */
  public specialRequest: string;

  /**
   * The id of the graduation event
   */
  public graduationEventId: number;


  /**
   * The graduation form
   */
  public studentRequest = {
    specialRequest: '',
    participateInCeremony: false,
    requirementsChecked: false
  };

  public errorMessage: string;

  constructor(private _translateService: TranslateService) { }


  ngOnInit() {

    this.graduationRequestStatusSubscription = this.graduationRequestStatusObservable$.subscribe((graduationStatus) => {
      this.handleGraduationStatus(graduationStatus);
    });
  }

  ngOnDestroy(): void {
    if (this.graduationRequestStatusSubscription) {
      this.graduationRequestStatusSubscription.unsubscribe();
    }
  }

  /**
   *
   * Given a graduation request status object, it updates the component's data
   *
   * @param graduationStatus The graduation request status object
   *
   */
  handleGraduationStatus(graduationStatus) {
    this.graduationRequestStatus = graduationStatus;
    const currentStep = this.getStep(this.graduationRequestStatus, this.alternateName);

    if (currentStep && currentStep.data) {
      this.submitted = !!currentStep.data.graduationRequest;
      this.graduationEventId = currentStep.data.graduationEvent.id;
    }

    if (currentStep.data.errors && currentStep.data.errors.graduationRequest) {
      this.errorMessage = this.createError(currentStep.data.errors.graduationRequest);
      console.log('this.errorMessage: ', this.errorMessage);
    }

      // assign a request name if there is not ones
    if (this.submitted) {
      this.requestName = currentStep.data.graduationRequest.name;
      this.specialRequest = currentStep.data.graduationRequest.description;
    } else {

      this.requestName = this._translateService.instant('UniversisGraduationModule.GraduationRequest.DefaultRequestName', {
        academicPeriod: this._translateService.instant(
          `UniversisGraduationModule.Semester.${this.graduationRequestStatus.academicPeriod}`
        ),
        academicYear: this.graduationRequestStatus.academicYear
      });

      this.graduationRequestConsents = [
        {
          id: 'participateInCeremony',
          translationKey: this._translateService.instant( 'UniversisGraduationModule.GraduationRequest.ParticipateInCeremony', {
            academicPeriod: this._translateService.instant(
              `UniversisGraduationModule.Semester.${this.graduationRequestStatus.academicPeriod}Possessive`
            ),
          }),
          required: true
        },
        {
          id: 'requirementsChecked',
          translationKey: 'UniversisGraduationModule.GraduationRequest.RequirementsChecked',
          required: true
        }
      ];
    }
  }

  /**
   *
   * Updates the status of a checkbox and decides if the form can be submitted
   *
   * @param event The HTML event that triggered the status change
   * @param index The index of the checkbox to update it's status
   *
   */
  updateConsentsStatus(event: any, index: number): void {
    this.consents[index] = event.target.checked;

    this.canSubmit = this.graduationRequestConsents.reduce((aggregator, consentItem, i) => {
      if (consentItem.required) {
        return aggregator && this.consents[i];
      } else {
        return aggregator;
      }
    }, true);
  }

  /**
   * submits the form
   */
  async submit() {
    const form = {
      name: this.requestName,
      specialRequest: this.studentGraduationRequest.value['graduation-special-request'],
      participateInCeremony: this.studentGraduationRequest.value.participateInCeremony,
      requirementsChecked: this.studentGraduationRequest.value.requirementsChecked,
    };

    this.formSubmit.emit({
      form,
      graduationEventId: this.graduationEventId
    });
  }

  /**
   *
   * Creates the translated error message
   *
   * @param errorCode The error code that was given by the server
   *
   */
  createError(errorCode?: string): string {
    let error: string;
    if (errorCode) {
      error = this._translateService.instant(`UniversisGraduationModule.GraduationRequest.errors.${errorCode}`);
    } else {
      error = this._translateService.instant(`UniversisGraduationModule.GraduationRequest.errors.generic`);
    }

    return error;
  }

  /**
   *
   * Given an alternateName, gets the the step
   *
   * @param graduationRequestStatus The graduation request status data
   * @param alternateName The alternate name of the step
   *
   */
  getStep(graduationRequestStatus, alternateName: string) {
    return graduationRequestStatus.steps.find(
      (step) => new RegExp(alternateName, 'gi').test(step.alternateName)
    );
  }
}
