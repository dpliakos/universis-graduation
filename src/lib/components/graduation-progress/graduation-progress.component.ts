import { Component, OnInit } from '@angular/core';
import { LoadingService } from '@universis/common';
import { GraduationRequestService } from './../../services/graduation-request/graduation-request.service'
import { GraduationGradesService } from './../../services/graduation-grades/graduation-grades.service';

@Component({
  selector: 'universis-graduation-progress',
  templateUrl: './graduation-progress.component.html',
  styleUrls: ['./graduation-progress.component.scss']
})
export class GraduationProgressComponent implements OnInit {
  public messageRes: void;
  public courseTypes: any;
  public studentSpecialty: any;
  public studentGuide: any;
  public isLoading = true;
  public graduationInfo: any;

  constructor(private _loadingService: LoadingService,
              private _graduationGradesService: GraduationGradesService,
              private _graduationRequestService: GraduationRequestService) {}

  async ngOnInit() {
    try {
      this._loadingService.showLoading();
      const getStudent = this._graduationRequestService.getStudent();
      const getGraduationRules = this._graduationGradesService.getGraduationRules();
      const getCourseTypes = this._graduationGradesService.getCourseTypes();
      const [student, graduationInfo, courseTypes] = await Promise.all([getStudent, getGraduationRules, getCourseTypes]);

      this.courseTypes = courseTypes;
      this.graduationInfo = graduationInfo;
      this.studentGuide = student.studyProgram.name;
      this.studentSpecialty = student.specialty;
    } catch (err) {
      console.warn(err);
    } finally {
      this._loadingService.hideLoading();
      this.isLoading = false;
    }
  }

}
