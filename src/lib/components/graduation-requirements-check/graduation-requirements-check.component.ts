import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Subscription } from 'rxjs/internal/Subscription';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'universis-graduation-requirements-check',
  templateUrl: './graduation-requirements-check.component.html'
})
export class GraduationRequirementsCheckComponent implements OnInit, OnDestroy {

  public alternateName = 'graduationRequirementsCheck';

  /**
   * The overall graduation request status
   */
  @Input() graduationRequestStatusObservable$: BehaviorSubject<any>;

  /**
   * Subscription to the graduation status
   */
  graduationRequestStatusSubscription: Subscription;

  /**
   * The overall graduation request status
   */
  public graduationRequestStatus;

  public courseTypes: any;
  public graduationInfo: any;
  public studentGuide: any;
  public studentSpecialty: any;

  /**
   *
   * A message for the status of the overall process
   *
   */
  public statusMessage;


  constructor(
    private _translateService: TranslateService
  ) { }


  ngOnInit() {
    this.graduationRequestStatusSubscription = this.graduationRequestStatusObservable$.subscribe((graduationStatus) => {
      this.graduationRequestStatus = graduationStatus;

      const currentStep = this.getStep(this.graduationRequestStatus, this.alternateName);

      if (currentStep && currentStep.data) {
        this.courseTypes = currentStep.data.courseTypes;
        this.graduationInfo = currentStep.data.graduationInfo;
        this.studentGuide = currentStep.data.student.studyProgram.name;
        this.studentSpecialty = currentStep.data.student.specialty;

        if (currentStep.status) {
          this.statusMessage = this._translateService.instant(
            `UniversisGraduationModule.RequirementsCheck.Status.${currentStep.status}`
          );
        } else {
          this.statusMessage = undefined;
        }
      }
    });
  }

  ngOnDestroy(): void {
    if (this.graduationRequestStatusSubscription) {
      this.graduationRequestStatusSubscription.unsubscribe();
    }
  }

  /**
   *
   * Given an alternateName, gets the the step
   *
   * @param graduationRequestStatus The graduation request status data
   * @param alternateName The alternate name of the step
   *
   */
  getStep(graduationRequestStatus, alternateName: string) {
    return graduationRequestStatus.steps.find(
      (step) => new RegExp(alternateName, 'gi').test(step.alternateName)
    );
  }
}
