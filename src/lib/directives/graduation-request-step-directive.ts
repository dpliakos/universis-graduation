import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[universisGraduationRequestWizard]',
})
export class GraduationRequestWizardDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}

