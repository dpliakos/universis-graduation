import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { HttpClient } from '@angular/common/http';
import { StudentService } from './../student/student.service';
import { GraduationGradesService } from './../graduation-grades/graduation-grades.service';
import { GraduationRequestStatus } from './../../graduation';
import { GraduationRequestStep } from './../../graduation-request-step';
import {
  GraduationRequestFormComponent
} from './../../components/graduation-request-form/graduation-request-form.component';
import {
  GraduationRequirementsCheckComponent
} from './../../components/graduation-requirements-check/graduation-requirements-check.component';
import {
  GraduationDocumentSubmissionComponent
} from './../../components/graduation-document-submission/graduation-document-submission.component';
import {
  GraduationCeremonyComponent
} from './../../components/graduation-ceremony/graduation-ceremony.component';


/**
 *
 * Handles data between api and the students app regarding the graduation process
 *
 */

@Injectable({
  providedIn: 'root'
})
export class GraduationRequestService {

  constructor(
    private _context: AngularDataContext,
    private _httpClient: HttpClient,
    private _graduationGradesService: GraduationGradesService,
    private _studentService: StudentService
  ) { }


  /**
   *
   * Test a request action status
   *
   * @param request The request to test against the filers
   *
   */
  isEligibleRequest(request): boolean {
    // TODO: check if there is this implementation in common
    const isCanceledRegExp = new RegExp('CancelledActionStatus', 'gi');
    const isFailedRegExp = new RegExp('FailedActionStatus', 'gi');

    if (!request || !request.actionStatus) {
      return false;
    }

    const isCanceled = isCanceledRegExp.test(request.actionStatus.alternateName);
    const isFailed = isFailedRegExp.test(request.actionStatus.alternateName);

    return !isCanceled && !isFailed;
  }

  /**
   *
   * Decides if there is an available graduation event
   *
   * @param graduationEvent The graduation event as fetched from the server
   *
   */
  graduationPeriodExist(graduationEvent) {
    return graduationEvent
      && !!graduationEvent.startDate
      && !!graduationEvent.endDate;
  }

  /**
   *
   * Decides if the graduation period is open
   *
   * @param graduationEvent The graduation event as fetched from the server
   *
   */
  isGraduationPeriodOpen(graduationEvent) {
    const now = new Date();
    const graduationPeriodExist = this.graduationPeriodExist(graduationEvent);

    return graduationPeriodExist
      ? now >= graduationEvent.validFrom
        && now <= graduationEvent.validThrough
        && !!graduationEvent.eventStatus
        && new RegExp('EventOpened', 'gi').test(graduationEvent.eventStatus.alternateName)
      : false;
  }

  /**
   *
   * Decides the state of the document submission
   *
   */
  getDocumentsSubmissionStatus(attachmentsStatus): string {
    if (!attachmentsStatus || attachmentsStatus.length === 0) {
      return 'unavailable';
    } else if (attachmentsStatus.every(attachment => attachment.status === 'completed')) {
      return 'completed';
    } else {
      return 'pending';
    }
  }

  /**
   *
   * Decides the graduation request action (document) status
   *
   * @param requestAction The graduation request action object
   *
   */
  getRequestActionStatus(requestAction): string {
    if (requestAction) {
      return 'completed';
    } else {
     return 'pending';
    }
  }

  /**
   *
   * Decides the status of the graduation requirements check
   *
   * @param graduationRules The graduation rules response from the api
   *
   */
  getGraduationRequirementsCheckStatus(graduationRules) {
    if (!graduationRules.finalResult) {
      return 'unavailable';
    } else if (graduationRules.finalResult.success) {
      return 'completed';
    } else {
      return 'pending';
    }

  }

  /**
   *
   * Matches a graduation request action with a graduation request action type.
   *
   * @param requestActionTypes The list of available action types
   * @param requestActions The list of the request actions (documents)
   *
   */
  getDocuments(requestActionTypes: Array<any>, requestActions: Array<any>) {
    const filteredRequestActionTypes = requestActionTypes.filter((actionType) => !!actionType.name && !!actionType.alternateName);
    if (filteredRequestActionTypes.length !== requestActionTypes.length) {
      console.warn('Graduation action type(s) without name or alternateName found and removed');
    }

    const requestDocumentsStatus = filteredRequestActionTypes.map((requestActionType) => {
      const selectedRequestAction = requestActions.find(
        (requestAction) => requestAction.attachmentType === requestActionType.attachmentType
      );

      return {
        alternateName: requestActionType.alternateName,
        name: requestActionType.name,
        description: requestActionType.description,
        document: selectedRequestAction,
        status: this.getRequestActionStatus(selectedRequestAction),
        requestActionType
      };
    });

    return requestDocumentsStatus;
  }

  /**
   *
   * Creates a bundle with information regarding the graduation process
   * Returns the overview of the graduation process
   *
   */
  async getGraduationRequestStatus(): Promise<GraduationRequestStatus> {
    // available statuses: 'unavailable', 'available', 'submitted', 'pending'

    let student;
    let availableGraduationEvent;
    let graduationRequest ;
    let graduationRules;
    let courseTypes;

    try {
      [student, availableGraduationEvent, graduationRequest, graduationRules, courseTypes] = await Promise.all([
        this.getStudent(),
        this.getAvailableGraduationEvent(),
        this.getGraduationRequest(),
        this.getGraduationRules(),
        this._graduationGradesService.getCourseTypes()
      ]);
    } catch (err) {
      console.warn(err);
      student = availableGraduationEvent = graduationRequest = graduationRules = courseTypes = undefined;
    }
    
    // When there is not graduation event, the available graduation event is being used (misses attachment data)
    const graduationEvent = graduationRequest ? graduationRequest.graduationEvent : availableGraduationEvent;

    const graduationPeriodExist = this.graduationPeriodExist(graduationEvent);
    const attachmentsStatus = this.getDocuments(
      graduationRequest && graduationRequest.attachmentTypes ? graduationRequest.attachmentTypes : [],
      graduationRequest && graduationRequest.attachments ? graduationRequest.attachments : []
    );

    const graduationRequestStepStatus = this.getGraduationRequestStepStatus(graduationPeriodExist, graduationRequest);
    const graduationRulesStepStatus = this.getGraduationRulesStepStatus(!!graduationRequest, graduationRules);
    const graduationDocumentsSubmissionStatus = this.getDocumentsSubmissionStepStatus(
      attachmentsStatus,
      graduationRulesStepStatus === 'completed'
    );
    const graduationCeremonyStepStatus = this.getGraduationCeremonyStepStatus(
      graduationRequest ? graduationRequest.actionStatus.alternateName : undefined,
      graduationRequest && graduationRequest.attachmentTypes.length > 0, // decides graduation documents submission is required
      graduationDocumentsSubmissionStatus === 'completed'
    );

    const graduationRequestStep = this.getGraduationRequestStep(graduationRequestStepStatus, {
      graduationRequest,
      graduationEvent
    });

    const graduationRequirementsCheck = this.getGraduationRequirementsCheckStep(graduationRulesStepStatus, {
      student,
      graduationInfo: graduationRules,
      courseTypes
    });

    const graduationDocumentsSubmissionStep = this.getGraduationDocumentsSubmissionStep(graduationDocumentsSubmissionStatus, {
      documentsSubmissionStatus: this.getDocumentsSubmissionStatus(attachmentsStatus),
      attachments: attachmentsStatus,
      graduationRequestId: graduationRequest ? graduationRequest.id : undefined
    });

    const graduationCeremonyStep = this.getGraduationCeremonyStep(graduationCeremonyStepStatus, {
      location: graduationEvent ? graduationEvent.location : undefined,
      ceremonyDate: graduationEvent ? graduationEvent.startDate : undefined,
      certificates: [],
      failedMessage: '' // A descriptive message for the student in case of failed graduation (e.g. didn't attend graduation ceremony)
    });

    const status =  {
      status: graduationRequest && graduationRequest.actionStatus.alternateName ? graduationRequest.actionStatus.alternateName : undefined,
      graduationPeriodExist,
      graduationRequestPeriodOpen: this.isGraduationPeriodOpen(graduationEvent),
      academicYear: student.department.currentYear.alternateName,
      academicPeriod: student.department.currentPeriod.alternateName,
      requestPeriodStart: graduationEvent ? graduationEvent.validFrom : undefined,
      requestPeriodEnd: graduationEvent ? graduationEvent.validThrough : undefined,
      steps: [
        graduationRequestStep,
        graduationRequirementsCheck,
        graduationCeremonyStep
      ]
    };

    if  (graduationRequest && graduationRequest.attachmentTypes &&  graduationRequest.attachmentTypes.length > 0) {
      status.steps.splice(2, 0, graduationDocumentsSubmissionStep);
    }

    return status;
  }

  /**
   *
   * Gets information regarding the student
   *
   */
  async getStudent() {
    return this._context.model('students/me')
      .asQueryable()
      .expand('user', 'department', 'studyProgram', 'inscriptionMode', 'person($expand=gender)')
      .getItem().then(student => {
        if (typeof student === 'undefined') {
          return Promise.reject('Student data cannot be found');
        }
        return this._context.model('LocalDepartments')
          .asQueryable()
          .where('id').equal(student.department.id)
          .expand('organization($expand=instituteConfiguration,registrationPeriods)')
          .getItem().then(department => {
            if (typeof department === 'undefined') {
              return Promise.reject('Student department cannot be found');
            }
            student.department = department;
            sessionStorage.setItem('student', JSON.stringify(student));
            return Promise.resolve(student);
          });
      });
  }

  /**
   *
   * Get the student's graduation request
   *
   */
  async getGraduationRequest() {
    const graduationRequests = await this._context.model('students/me/graduationRequests')
      .asQueryable()
      .expand('graduationEvent($expand=attachmentTypes($expand=attachmentType),eventStatus),attachments')
      .getItems();

    if (!graduationRequests || graduationRequests.length === 0) {
     return undefined;
    }

    // Filter out canceled and failed actions.
    const filteredGraduationRequests = graduationRequests.filter((request) => this.isEligibleRequest(request));
    if (filteredGraduationRequests.length >= 2) {
      console.warn('More than one graduation requests found');
    }

    if (filteredGraduationRequests.length === 0) {
      return undefined;
    } else {
      return graduationRequests[0];
    }
  }

  /**
   * Uploads a graduation attachment
   */
  async uploadGraduationRequestAttachment(attachmentData) {
    const attachmentTypeAlternateName = attachmentData.attachmentType;
    const headers = {...this._context.getService().getHeaders()};
    try {
      const url = this._context.getService().resolve(`students/me/requests/${attachmentData.eventId}/attachments/add`);
      await this._httpClient.post(url, attachmentData.form, {
        headers
      }).toPromise();

      return await this.getGraduationRequestStatus();
    } catch (err) {
      const newStatus = await this.getGraduationRequestStatus();
      if (!newStatus.steps[2].data.errors) {
        newStatus.steps[2].data.errors = {};
      }
      newStatus.steps[2].data.errors.Upload = {};
      newStatus.steps[2].data.errors.Upload[attachmentTypeAlternateName] = err;
      return newStatus;
    }

  }

  /**
   *
   * Remove a graduation request document
   *
   * @param requestId The id of the request
   * @param attachmentId  The id of the attachment
   *
   */
  async removeGraduationRequestAttachment(data) {
    const requestId = data.requestId;
    const attachmentId = data.attachmentId;
    const attachmentTypeAlternateName = data.attachmentTypeAlternateName;
    try {
      await this._context.model(`students/me/requests/${requestId}/attachments/${attachmentId}/remove`).save({});
      return await this.getGraduationRequestStatus();
    } catch (err) {
      const newStatus = await this.getGraduationRequestStatus();
      if (!newStatus.steps[2].data.errors) {
        newStatus.steps[2].data.errors = {};
      }
      newStatus.steps[2].data.errors.Remove = {};
      newStatus.steps[2].data.errors.Remove[attachmentTypeAlternateName] = err;
      return newStatus;
    }
  }

  /**
   *
   * Downloads the a graduation request attachment
   *
   * @param data The data that contains information about the file
   *
   */
  async downloadGraduationRequestAttachment(data): Promise<any> {
    const attachmentAlternateName = data.attachmentAlternateName;
    const attachmentTypeAlternateName = data.attachmentTypeAlternateName;

    try {
      const headers = this._context.getService().getHeaders();
      const base = this._context.getBase();
      const file = await this._httpClient.get(`${base}content/private/${attachmentAlternateName}`, {
        responseType: 'blob',
        headers
      }).toPromise();

      this.triggerFileDownloadPopup(file, attachmentAlternateName);
      return await this.getGraduationRequestStatus();
    } catch (err) {
      const newStatus = await this.getGraduationRequestStatus();

      if (!newStatus.steps[2].data.errors) {
        newStatus.steps[2].data.errors = {};
      }
      newStatus.steps[2].data.errors.Download = {};
      newStatus.steps[2].data.errors.Download[attachmentTypeAlternateName] = err;

      return newStatus;
    }
  }

  /**
   *
   * Creates a graduation request for a student
   *
   * @param graduationEvent The graduation event for which the student is applying
   * @param name The special request from the student
   *
   */
  async setGraduationRequest(graduationEventId: number, graduationForm) {
    try {
      await this._context.model(`graduationRequestActions`).save({
        graduationEvent: {
          id: graduationEventId
        },
        name: graduationForm.name,
        description: graduationForm.specialRequest
      });

      const status = await this.getGraduationRequestStatus();
      return status;
    } catch (err) {
      const status = await this.getGraduationRequestStatus();
      if (!status.steps[0].data) {
        status.steps[0].data = {};
      }

      if (!status.steps[0].data.errors) {
        status.steps[0].data.errors = {};
      }

      status.steps[0].data.errors.graduationRequest = err.error && err.error.code
        ? err.error.code
        : err;
      return status;
    }
  }

  /**
   *
   * Fetches the available event status
   *
   */
  async getAvailableGraduationEvent() {
    return this._context.model('students/me/availableGraduationEvents')
      .asQueryable()
      .getItem();
  }

  /**
   *
   * Gets the graduation rules for a student
   *
   */
  async getGraduationRules() {
    return this._context.model('students/me/graduationRules')
      .asQueryable()
      .expand('validationResult')
      .take(-1)
      .getItems();
  }

  /**
   *
   * Resolves alternateNames to angular components
   *
   * @param alternateName The alternate name of the component
   *
   */
  getComponent(alternateName: string): GraduationRequestStep {
    switch (alternateName) {
      case 'graduationRequest': return new GraduationRequestStep(GraduationRequestFormComponent, {});
      case 'graduationRequirementsCheck': return new GraduationRequestStep(GraduationRequirementsCheckComponent, {});
      case 'graduationDocumentsSubmission': return new GraduationRequestStep(GraduationDocumentSubmissionComponent, {});
      case 'graduationCeremony': return new GraduationRequestStep(GraduationCeremonyComponent, {});
    }
  }

  /**
   *
   * triggers a file download popup at the browser
   *
   * @param blob The file as received from the server
   */
  triggerFileDownloadPopup(blob: Blob, fileName: string) {
    const objectUrl = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    document.body.appendChild(a);
    a.setAttribute('style', 'display: none');
    a.href = objectUrl;
    const extension = 'txt'; // TODO: change the extension
    a.download = `${fileName}-.${extension}`;
    a.click();
    window.URL.revokeObjectURL(objectUrl);
    a.remove();
  }

  // The following functions are ad-hoc implementation and should be encapsulated
  // inside a GraduationRequestStep data   structure. An abstraction over them is a
  // nice to have feature

  /**
   *
   * Gets data for the graduation request
   *
   * @param status The status of the current component
   * @param data The data to pass to the component
   *
   */
  getGraduationRequestStep(status: string, data) {
    if (!data.errors) {
      data.errors = {};
    }

    return {
      title: 'UniversisGraduationModule.GraduationRequest.Title',
      alternateName: 'graduationRequest',
      component: this.getComponent('graduationRequest'),
      status,
      data
    };
  }

  getGraduationRequirementsCheckStep(status: string, data) {
    if (!data.errors) {
      data.errors = {};
    }

    return {
      title: 'UniversisGraduationModule.RequirementsCheck.Title',
      alternateName: 'graduationRequirementsCheck',
      component: this.getComponent('graduationRequirementsCheck'),
      status,
      data
    };
  }

  getGraduationDocumentsSubmissionStep(status: string, data) {
    if (!data.errors) {
      data.errors = {};
    }

    return {
      title: 'UniversisGraduationModule.DocumentsSubmission.Title',
      alternateName: 'graduationDocumentsSubmission',
      component: this.getComponent('graduationDocumentsSubmission'),
      status,
      data
    };
  }

  getGraduationCeremonyStep(status: string, data) {
    if (!data.errors) {
      data.errors = {};
    }

    return {
      title: 'UniversisGraduationModule.GraduationCeremony.Title',
      alternateName: 'graduationCeremony',
      component: this.getComponent('graduationCeremony'),
      status,
      data
    };
  }

  /**
   *
   * The graduation request is available only if there is not
   * a not failed or canceled request
   *
   * @param graduationPeriodExist A flag that states whether a graduation period exists
   * @param graduationRequest The graduation request of the student
   *
   */
  getGraduationRequestStepStatus(graduationPeriodExist: boolean, graduationRequest): string {
    if (!graduationPeriodExist) {
      return 'unavailable';
    } else if (graduationPeriodExist && graduationRequest) {
      return 'completed';
    } else {
      return 'available';
    }
  }

  /**
   *
   * The graduationRequirementsCheck step is available only if there is a graduation request.
   * It is unavailable if there is not a graduation request.
   * It is failed if there is at least a failed step.
   *
   * @param graduationRequestExists A flag that states whether a graduation request exists
   * @param graduationRules The graduation rules list
   *
   */
  getGraduationRulesStepStatus(graduationRequestExists: boolean, graduationRules) {
    if (!graduationRequestExists || !graduationRules.finalResult) {
      return 'unavailable';
    } else if (graduationRules.finalResult.success) {
      return 'completed';
    } else {
      return 'pending';
    }
  }

  /**
   *
   * The documents submission step is available only if the requirements check have passed
   * It is completed when the documents are accepted
   * It is failed if a document is rejected
   *
   * @param attachmentsStatus The status of the attachments
   *
   */
  getDocumentsSubmissionStepStatus(attachmentsStatus, graduationRulesCompleted: boolean) {
    if (!attachmentsStatus || attachmentsStatus.length === 0 || !graduationRulesCompleted) {
      return 'unavailable';
    } else if (attachmentsStatus.every(attachment => attachment.status === 'completed')) {
      return 'completed';
    } else {
      return 'pending';
    }
  }

   /**
    *
    * The graduation ceremony is available if the documents submission is completed
    * It is completed when there are graduation documents available
    *
    * @param graduationRequestStatus The status of the graduation request
    * @param documentSubmissionIsRequired A flag that indicates whether the document submission is required
    * @param documentSubmissionCompleted A flag that indicates whether the document submission is completed
    *
    */
  getGraduationCeremonyStepStatus(
    graduationRequestStatus: string,
    documentSubmissionIsRequired: boolean,
    documentSubmissionCompleted: boolean
    ) {
    if (!graduationRequestStatus) {
      return 'unavailable';
    }

    if (documentSubmissionIsRequired) {
      if (documentSubmissionCompleted) {
        return 'pending';
      } else {
        return 'unavailable';
      }
    } else {
      if (documentSubmissionCompleted) {
        return 'pending';
      } else {
        return 'unavailable';
      }
    }
  }
}
