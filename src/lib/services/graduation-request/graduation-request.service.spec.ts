import { TestBed } from '@angular/core/testing';
import { GraduationRequestService } from './graduation-request.service';

describe('GraduationRequestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ ]
    })
    .compileComponents();
  });

  it('should be created', () => {
    const service: GraduationRequestService = TestBed.get(GraduationRequestService);
    expect(service).toBeTruthy();
  });
});
