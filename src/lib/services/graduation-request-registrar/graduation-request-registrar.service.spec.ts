import { TestBed } from '@angular/core/testing';

import { GraduationRequestRegistrarService } from './graduation-request-registrar.service';

describe('GraduationRequestRegistrarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GraduationRequestRegistrarService = TestBed.get(GraduationRequestRegistrarService);
    expect(service).toBeTruthy();
  });
});
