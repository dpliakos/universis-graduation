import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { asyncMemoize } from '@universis/common';


@Injectable({
  providedIn: 'root'
})
export class GraduationGradesService {

  constructor(private _context: AngularDataContext) { }

  /**
   *
   * Gets information regarding the graduation rules
   *
   */
  getGraduationRules(): any {
    return this._context.model('students/me/graduationRules')
      .asQueryable()
      .expand('validationResult')
      .take(-1)
      .getItems();
  }
/**
 *
 * Gets information regarding the course types
 *
 */
  @asyncMemoize()
  getCourseTypes(): any {
    return this._context.model('courseTypes')
      .take(-1)
      .getItems();
  }
}
