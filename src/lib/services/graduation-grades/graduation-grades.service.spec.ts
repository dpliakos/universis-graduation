import { TestBed } from '@angular/core/testing';

import { GraduationGradesService } from './graduation-grades.service';

describe('GraduationGradesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GraduationGradesService = TestBed.get(GraduationGradesService);
    expect(service).toBeTruthy();
  });
});
