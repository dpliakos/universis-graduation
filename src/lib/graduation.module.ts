import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgPipesModule } from 'ngx-pipes';
import { SharedModule } from '@universis/common';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ConfigurationService } from '@universis/common';
import { GraduationRoutingModule } from './graduation.router';
import { environment } from './environments/environment';
import { StudentService } from './services/student/student.service';
import { GraduationRequestService } from './services/graduation-request/graduation-request.service';
import { GraduationRequestComponent } from './components/graduation-request/graduation-request.component';
import { GraduationRequestFormComponent } from './components/graduation-request-form/graduation-request-form.component';
import { GraduationRequirementsCheckComponent } from './components/graduation-requirements-check/graduation-requirements-check.component';
import {
  GraduationDocumentSubmissionComponent
} from './components/graduation-document-submission/graduation-document-submission.component';
import { GraduationCeremonyComponent } from './components/graduation-ceremony/graduation-ceremony.component';
import { GraduationRequestWizardDirective } from './directives/graduation-request-step-directive';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { GraduationProgressComponent } from './components/graduation-progress/graduation-progress.component';
import { GraduationRulesComponent } from './components/graduation-rules/graduation-rules.component';
import { GraduationStudentsLayoutComponent } from './components/graduation-students-layout/graduation-students-layout.component';
import { GraduationStudentsHomeComponent } from './components/graduation-students-home/graduation-students-home.component';
import {
  GraduationStudentsGraduationTabComponent
} from './components/graduation-students-graduation-tab/graduation-students-graduation-tab.component';
import {
  GraduationStudentsRequestWrapperComponent
} from './components/graduation-students-request-wrapper/graduation-students-request-wrapper.component';
import { 
  GraduationStudentsRulesWrapperComponent 
} from './components/graduation-students-rules-wrapper/graduation-students-rules-wrapper.component';

import {GRADUATION_LOCALES} from './i18n';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    TranslateModule,
    GraduationRoutingModule,
    NgPipesModule,
    SharedModule.forRoot()
  ],
  providers: [
    StudentService,
    GraduationRequestService,
    TranslateService,
    ConfigurationService,
    FormBuilder
  ],
  declarations: [
    GraduationRequestComponent,
    GraduationRequestFormComponent,
    GraduationRequirementsCheckComponent,
    GraduationDocumentSubmissionComponent,
    GraduationCeremonyComponent,
    GraduationRequestWizardDirective,
    GraduationProgressComponent,
    GraduationRulesComponent,
    GraduationStudentsLayoutComponent,
    GraduationStudentsHomeComponent,
    GraduationStudentsGraduationTabComponent,
    GraduationStudentsRequestWrapperComponent,
    GraduationStudentsRulesWrapperComponent
  ],
  entryComponents: [
    GraduationRequestFormComponent,
    GraduationRequirementsCheckComponent,
    GraduationDocumentSubmissionComponent,
    GraduationCeremonyComponent
  ]
})
export class GraduationModule {
  constructor(private _translateService: TranslateService) {
    environment.languages.forEach((culture) => {
      this._translateService.setTranslation(culture, GRADUATION_LOCALES[culture], true);
    });
  }
}
