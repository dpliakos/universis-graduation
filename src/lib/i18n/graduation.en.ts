/* tslint:disable max-line-length */
export const en = {
  UniversisGraduationModule: {
    Degree: 'Degree',
    DegreePrerequisites: 'Degree prerequisites',
    Graduation: 'Graduation',
    GraduationRequestWizard: 'Graduation request wizard',
    BackToDegree: 'Back to degree',
    GraduationDatesNotice: 'Graduation request period will be open from {{dateStart}} to {{dateEnd}}.',
    OutOfGraduationRequestPeriodMessage: 'Graduation request period is not open. A reminder email with the date will be sent at your academic email.',
    InGraduationRequestPeriodMessage: 'Graduation request period  for {{semesterPeriod}} semester {{academicYear}} is open. You can use the graduation request wizard to submit the graduation request.',
    GraduationRequestSubmittedMessage: 'You can use the graduation request wizard to check your graduation request progress.',
    Semester: {
      title: 'Semester',
      summer: 'summer',
      winter: 'winter',
      summerPossessive: 'summer',
      winterPossessive: 'winter',
      caps: {
        title: 'SEMESTER',
        winter: 'WINTER',
        summer: 'SUMMER'
      }
    },
    GraduationRequestTitle: 'Graduation request',
    SpecialRequest: {
      Title: 'Special request',
      Notice: 'If you need to make a special request, write your message at the field below.',
      InputPlaceholder: 'Your message to registrar'
    },
    GraduationRequest: {
      Title: 'Graduation request',
      DefaultRequestName: 'I request to submit graduation request for {{ academicPeriod }} semester {{academicYear}}.',
      RequirementsChecked: 'I agree that I will participate in graduation ceremony of {{academicPeriod}} semester',
      ParticipateInCeremony: 'I have checked that the requirements  are fulfilled',
      WillAnnounceDatesNotice:  'The graduation ceremony dates and location will be announced after the graduation request period',
      Request: 'Request',
      errors: {
        generic: 'There where an error during the request submission.',
        EUNQ: 'You have already submitted a graduation request for this graduation event.'
      }
    },
    RequirementsCheck: {
      Title: 'Requirements Check',
      StatusCheck: 'Status check',
      DegreeRequirements: 'Degree requirements',
      Status: {
        unavailable: 'The graduation requirements check has not started.',
        failed: 'Graduation requirements are not fulfilled.',
        pending: 'The graduation requirements check is ongoing.',
        completed: 'The graduation requirements check is completed successfully.'
      }
    },
    DocumentsSubmission: {
      Title: ' Documents submission',
      Subtitle: ' Follow the instructions in order to submit the required documents for your graduation.',
      SubmissionStatus: 'Submission status',
      SubmissionStatuses: {
        pending: 'Document submission for graduation is ongoing.',
        completed: 'Document submission for graduation is completed',
        failed: 'Document submission for graduation has failed'
      },
      AttachmentDeleteModal: {
        Title: 'Document delete',
        Body: 'Delete document of {{attachmentType}} type?',
        Notice: 'This action can not be undone.',
        Close: 'Close',
        Delete: 'Delete'
      },
      GraduationDocuments: 'Graduation documents',
      DownloadDocument: 'Download document',
      UploadDocument: 'Upload document',
      RemoveDocument: 'Remove document',
      ContactService: 'contact related service',
      Errors: {
        Download: 'There was an error during the file download',
        Remove: 'There was an error during the file removal',
        Upload: 'There was an error during the file upload'
      }
    },
    GraduationCeremony: {
      Title: 'Graduation ceremony',
      ParticipationStatus: 'Participation Status',
      GraduationCeremonyStatus: 'Graduation ceremony status',
      GraduationCertificates: 'Graduation certificates',
      ContactRegistrar: 'Contact registrar',
      DatesWillBeAnnouncedNotice: 'The Date and the Location of the graduation ceremony will be announced after the graduation request period.',
      MailWillBeSentNotice: 'You will receive relevant message at your email',
      MandatoryParticipation: 'Your participation at the graduation event is mandatory.'
    },
    MessagePrompt: 'Your message',
    Send: 'Send',
    Cancel: 'Cancel',
    Date: 'Date',
    Time: 'Time',
    Location: 'Location',
    Download: 'Download',
    Previous:  'Previous',
    Next: 'Next',
    GraduationRules: 'Graduation Rules',
    GraduationRulesTab: 'Graduation',
    GraduationProgress: 'Graduation Progress',
    ComplexRules: 'The Department has complex graduation rules. This feature will be added in a later version.',
    ContactRegistrar: 'Contact Registrar',
    GraduationInfo: 'For more information about graduation rules you can check your studies guide or contact Registrar.',
    StudentInfo: 'Student Information',
    StudyGuide: 'STUDY GUIDE',
    Specialty : 'SPECIALTY',
    Prerequisites: 'Prerequisites',
    Progress: 'Progress',
    NoRulesFound: 'Graduation Rules have not been set.',
    CourseType: 'Course Type',
    AllTypeCourses: 'All Type Courses',
    Thesis: 'Thesis',
    Student: 'Semester',
    Internship: 'Internship',
    Course: 'Prerequisite Courses',
    CourseArea: 'Courses Area',
    CourseCategory: 'Courses Category',
    CourseSector: 'Courses Sector',
    ProgramGroup: 'Courses Group'
  }
};
