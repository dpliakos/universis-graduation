/*
 * Public API Surface of graduation
 */


export { GraduationModule } from './lib/graduation.module';
export { GraduationRoutingModule, graduationRequestRoutes } from './lib/graduation.router';
export { GraduationStudentsRoutingModule, studentRoutes } from './lib/graduation-students.router';
export { GRADUATION_LOCALES } from './lib/i18n/index';

// components
export { GraduationRequestComponent } from './lib/components/graduation-request/graduation-request.component';
export { GraduationRequestFormComponent } from './lib/components/graduation-request-form/graduation-request-form.component';
export { GraduationCeremonyComponent } from './lib/components/graduation-ceremony/graduation-ceremony.component';
export {
  GraduationRequirementsCheckComponent
} from './lib/components/graduation-requirements-check/graduation-requirements-check.component';
export {
  GraduationDocumentSubmissionComponent
} from './lib/components/graduation-document-submission/graduation-document-submission.component';
export { GraduationRequestService } from './lib/services/graduation-request/graduation-request.service';
export { GraduationProgressComponent } from './lib/components/graduation-progress/graduation-progress.component';
export { GraduationRulesComponent } from './lib/components/graduation-rules/graduation-rules.component';
export { GraduationStudentsLayoutComponent } from './lib/components/graduation-students-layout/graduation-students-layout.component';
export { GraduationStudentsHomeComponent } from './lib/components/graduation-students-home/graduation-students-home.component';
export {
  GraduationStudentsGraduationTabComponent
} from './lib/components/graduation-students-graduation-tab/graduation-students-graduation-tab.component';
export {
  GraduationStudentsRequestWrapperComponent
} from './lib/components/graduation-students-request-wrapper/graduation-students-request-wrapper.component';